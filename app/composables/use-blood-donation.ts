import { useDirectusItems } from '#imports'

interface BloodDonationStats {
  blood: BloodDonationType
  plasma: BloodDonationType
  platelets: BloodDonationType
  total: {
    count: number
    quantity: number
  }
  latest: {
    date: Date | null
    type: BloodDonationType['type'] | null
  }
}

interface BloodDonationType {
  type: 'blood' | 'plasma' | 'platelets'
  count: number
  sum: {
    quantity: number
  }
  max: {
    date: Date
  }
}

interface BloodDonation {
  id: string
  date: string
  type: BloodDonationType['type']
  quantity: number
  hematoma: boolean
}

const weeksToMilliseconds = (weeks: number): number =>
  weeks * 7 * 24 * 60 * 60 * 1000

export const useBloodDonation = () => {
  const { getItems } = useDirectusItems()

  const fetchHistory = async (): Promise<BloodDonation[]> => getItems<BloodDonation>({
    collection: 'blood_donation',
    params: {
      limit: 5,
      sort: ['-date'],
    },
  })

  interface BloodDonationStatsRow {
    type: BloodDonationType['type']
    count: string
    sum: { quantity: string }
    max: { date: string }
  }

  const fetchStats = async (): Promise<BloodDonationStatsRow[]> => getItems({
    collection: 'blood_donation',
    params: {
      aggregate: JSON.stringify({
        sum: 'quantity',
        max: 'date',
        count: '*',
      }),
      groupBy: 'type',
    },
  })

  const processRow = (row: BloodDonationStatsRow): BloodDonationType => ({
    type: row.type,
    count: parseInt(row.count, 10),
    sum: { quantity: parseInt(row.sum.quantity, 10) },
    max: { date: new Date(row.max.date) },
  })

  const aggregateStats = (acc: BloodDonationStats, row: BloodDonationType): BloodDonationStats => {
    acc[row.type] = row
    acc.total.count += row.count
    acc.total.quantity += row.sum.quantity
    if (!acc.latest.date || row.max.date > acc.latest.date) {
      acc.latest.date = row.max.date
      acc.latest.type = row.type
    }
    return acc
  }

  const processStats = (rows: BloodDonationStatsRow[]): BloodDonationStats => {
    const initialStats: BloodDonationStats = {
      blood: {} as BloodDonationType,
      plasma: {} as BloodDonationType,
      platelets: {} as BloodDonationType,
      total: { count: 0, quantity: 0 },
      latest: { date: null, type: null },
    }
    return rows
      .map(processRow)
      .reduce(aggregateStats, initialStats)
  }

  const calculateWeeksOld = (previousDonation: Date): number => {
    const today = new Date()
    return Math.floor(
      (today.getTime() - previousDonation.getTime()) / weeksToMilliseconds(1),
    )
  }

  const formatNextDonationDate = (previousDonation: Date, weeksToWait: number): string => {
    return new Date(previousDonation.getTime() + weeksToMilliseconds(weeksToWait))
      .toLocaleDateString('fr-FR', {
        weekday: 'long',
        year: 'numeric',
        month: 'short',
        day: 'numeric',
      })
  }

  return {
    fetchHistory,
    fetchStats,
    processStats,
    calculateWeeksOld,
    formatNextDonationDate,
  }
}
