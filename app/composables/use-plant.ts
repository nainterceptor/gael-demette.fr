import MarkdownIt from 'markdown-it'
import { useDirectusItems } from '#imports'

type LanguageCode = 'fr-FR' | 'en-US'

interface Translation {
  languages_code: LanguageCode
  name?: string
  summary?: string
  advice?: string
  feedback?: string
  description?: string
}

interface Translatable {
  translations: Translation[]
}

interface PlantCategory extends Translatable {
  id: string
  icon?: string
  name: string
  summary?: string
}

interface PlantDisease extends Translatable {
  code: string
  name: string
  description?: string
}

interface PlantFeedback extends Translatable {
  year: number
  feedback?: string
}

interface PlantPeriod {
  action: 'sow-under-cover' | 'sow-in-ground' | 'harvest'
  months: number[]
}

interface Plant extends Translatable {
  id: string
  average_weight: number
  needs: string[]
  distance_between_plants: number
  name: string
  summary?: string
  advice?: string
  category: PlantCategory
  disease_resistances: Array<{ plant_disease_id: PlantDisease }>
  feedbacks: PlantFeedback[]
  periods: PlantPeriod[]
}

export const usePlant = () => {
  const { getItemById, getItems } = useDirectusItems()
  const md = new MarkdownIt()

  const targetLang: LanguageCode = 'fr-FR'
  const fallbackLang: LanguageCode = 'en-US'
  const lang = [targetLang, fallbackLang] as const

  const resolveTranslation = function resolveTranslation<T extends Translatable>(this: T, ...keys: Array<keyof Translation>): Partial<T> {
    return keys.reduce(
      (acc, key) => ({
        ...acc,
        [key]: this.translations.find(t => t.languages_code === targetLang)?.[key]
          ?? this.translations.find(t => t.languages_code === fallbackLang)?.[key],
      }),
      {},
    )
  }

  const langFilter = { _filter: { languages_code: { _in: lang } } }
  const deep = {
    translations: langFilter,
    category: { translations: langFilter },
    disease_resistances: { plant_disease_id: { translations: langFilter } },
    feedbacks: { translations: langFilter },
  }

  const fields = [
    'id',
    'average_weight', 'needs', 'distance_between_plants',
    'category.id', 'category.icon', 'category.translations.languages_code', 'category.translations.name',
    'category.translations.summary',
    'translations.languages_code', 'translations.name', 'translations.advice', 'translations.summary',
    'feedbacks.year', 'feedbacks.translations.languages_code', 'feedbacks.translations.feedback',
    'periods.action', 'periods.months',
    'disease_resistances.plant_disease_id.code', 'disease_resistances.plant_disease_id.translations.languages_code',
    'disease_resistances.plant_disease_id.translations.name',
    'disease_resistances.plant_disease_id.translations.description',
  ]

  const fetchPlant = async (id: string) => {
    const rawPlant = await getItemById<Plant>({
      collection: 'plant',
      id: encodeURIComponent(id),
      params: { deep, fields },
    })

    const processedPlant = {
      ...rawPlant,
      average_weight: rawPlant.average_weight,
      needs: [].concat(rawPlant.needs).map(need => need.split('_')),
      distance_between_plants: rawPlant.distance_between_plants,
      ...resolveTranslation.call(rawPlant, 'name', 'summary', 'advice'),
      category: {
        id: rawPlant.category.id,
        icon: rawPlant.category.icon,
        ...resolveTranslation.call(rawPlant.category, 'name', 'summary'),
      },
      disease_resistances: rawPlant.disease_resistances.map(({ plant_disease_id: dr }) => ({
        code: dr.code,
        ...resolveTranslation.call(dr, 'name', 'description'),
      })),
      feedbacks: rawPlant.feedbacks.map(feedback => ({
        year: feedback.year,
        ...resolveTranslation.call(feedback, 'feedback'),
      })),
      periods: rawPlant.periods.map(period => ({
        action: period.action,
        months: period.months.map(m => +m).sort((a, b) => {
          if (a === Infinity) return 1
          if (Number.isNaN(a)) return -1
          return a - b
        }),
      })),
    }

    return {
      ...processedPlant,
      summary: processedPlant.summary ? md.render(processedPlant.summary) : processedPlant.summary,
      advice: processedPlant.advice ? md.render(processedPlant.advice) : processedPlant.advice,
      category: {
        ...processedPlant.category,
        summary: processedPlant.category.summary ? md.render(processedPlant.category.summary) : processedPlant.category.summary,
      },
      disease_resistances: processedPlant.disease_resistances.map(dr => ({
        ...dr,
        description: dr.description ? md.render(dr.description) : dr.description,
      })),
      feedbacks: processedPlant.feedbacks.map(feedback => ({
        ...feedback,
        feedback: feedback.feedback ? md.render(feedback.feedback) : feedback.feedback,
      })),
      periods: processedPlant.periods.map(period => ({
        ...period,
        months: Array.from({ length: 12 }, (_, i) => [i + 1, period.months.includes(i + 1)]),
      })),
    }
  }

  const getPlantCategories = async () => {
    const deep = {
      translations: langFilter,
    }
    const fields = [
      'id', 'location', 'icon',
      'translations.languages_code', 'translations.name', 'translations.summary',
    ]

    const rawCategories = await getItems<PlantCategory>({
      collection: 'plant_category',
      params: { deep, fields },
    })

    const processedCategories = rawCategories.map(category => ({
      id: category.id,
      location: category.location,
      icon: category.icon,
      ...resolveTranslation.call(category, 'name', 'summary'),
    }))

    return processedCategories.map(category => ({
      ...category,
      summary: category.summary ? md.render(category.summary) : category.summary,
    }))
  }

  const getPlantCategory = async (id: string) => {
    const deep = {
      translations: langFilter,
      plants: {
        translations: langFilter,
      },
    }
    const fields = [
      'id', 'icon',
      'translations.languages_code', 'translations.name', 'translations.summary',
      'plants.id', 'plants.average_weight', 'plants.needs', 'plants.distance_between_plants',
      'plants.translations.languages_code', 'plants.translations.name', 'plants.translations.summary',
    ]

    const rawCategory = await getItemById<PlantCategory & { plants: Plant[] }>({
      collection: 'plant_category',
      id: encodeURIComponent(id),
      params: { deep, fields },
    })

    const processedCategory = {
      id: rawCategory.id,
      icon: rawCategory.icon,
      ...resolveTranslation.call(rawCategory, 'name', 'summary'),
      plants: rawCategory.plants.map(plant => ({
        id: plant.id,
        average_weight: plant.average_weight,
        needs: [].concat(plant.needs).map(need => need.split('_')),
        distance_between_plants: plant.distance_between_plants,
        ...resolveTranslation.call(plant, 'name', 'summary'),
      })),
    }

    return {
      ...processedCategory,
      summary: processedCategory.summary ? md.render(processedCategory.summary) : processedCategory.summary,
      plants: processedCategory.plants.map(plant => ({
        ...plant,
        summary: plant.summary ? md.render(plant.summary) : plant.summary,
      })),
    }
  }

  return {
    fetchPlant,
    getPlantCategories,
    getPlantCategory,
  }
}
